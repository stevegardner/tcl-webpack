import _ from 'lodash';
import data from './data.json';

const message = document.createElement('p');
const text = `${data.greeting} from a webpack bundle`;

message.textContent = _.capitalize(text);
document.querySelector('body').append(message);
