# 01-basic-webpack

We’re building bundles! This no-config Webpack gets us off to a great start. We can now import `lodash` without including as a global variable.

This method follows a few conventions to work:

1. Our main file to our JavaScript code must be called `index.js`. This is the root file that should not be depended on by any other files.
1. Bundles will be named `main.js` and created in an output directory called `dist`.
1. Only loads JavaScript and JSON
