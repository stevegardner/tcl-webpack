import _ from 'lodash';
import babyYodaSrc from './images/baby-yoda.jpg';

import './message.css';

const container = document.createElement('div');
const message = document.createElement('p');
const text = 'hello from a webpack bundle';

const image = document.createElement('img');
image.src = babyYodaSrc;

message.textContent = _.capitalize(text);
container.append(message);
container.append(image);
document.querySelector('body').append(container);
