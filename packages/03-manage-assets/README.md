# 03-manage-assets

So far we‘ve talked about the first two items in a Webpack config that you‘ll see: `entry` and `output`. Next we‘ll dive into `module` which defines how Webpack should handle different file types and load them to the browser. We‘re also going to be using Webpack to break up our bundles and manage all file types.

## `module` config

The module field in your Webpack config lets you tell Webpack how to handle and load different file types. The [module API](https://webpack.js.org/configuration/module) could get even more attention. There's a lot we can do for loading different assets and files. For now, we'll cover the basics of getting CSS and images into our app.

The method Webpack uses for resolving file types specified in `module.rules` is using loader functions. Loaders run on matched file types and allow Webpack to incorporate those files into the project.

### CSS

Before, we were manually adding a link tag to our `index.html` to add styles. This affected all documents, even if `message.js` is never loaded. Let‘s only use those styles if we really need to. Using CSS requires the `css-loader` to wrap the CSS in JS and the `style-loader` to inject that CSS within a style element in the DOM.

**Check out webpack output**

### Images

We put our images in the `src` directory. Webpack can then manage our image assets. This means that images are hashed and we don’t have to worry about unique names ourselves to bust the browser cache. Imagine we add a new version of the image, but we want it to be use the same name as the old image in source. Without Webpack, the browser would use its cache since the filenames are the same. Webpack gives a unique hash filename for the built output.

Images are a type of asset. Previous to Webpack 5, we'd need to choose between loaders such as `file-loader` (return the path of the file within the server context—good for large files), `url-loader` (return a data URI—good for small files), or `raw-loader` (return that stringified version of the asset). These loader are available out-of-the-box in modern Webpack.

Now we can use `type: <asset-type>` in Webpack 5. For our images, we define `type: 'assets'`. Webpack will then choose between `file-loader` and `url-loader` depending on the size. We could also use `type: 'asset/resouce'` to always use `file-loader`.

### New `output` fields

We added `publicPath` and `assetModuleFilename` to our output config. `publicPath` defines where our files are for the server. Since we’re serving `index.html` from the root, we need to make sure images are prefixed with `build/` since that‘s where they‘re kept.

`assetModuleFilname` is just what it sounds like. We define where assets that are loaded with `type: 'asset'` and its variations get named. In this case, we want to save them in a separate folder. The default naming scheme uses a hash, but we can alter the name using various [placeholders](https://webpack.js.org/loaders/file-loader/#placeholders). Later, we‘ll use the original filename when developing and the hash in production.

## Vendor bundle

Another thing we got with webpack is having a clear indication of what 3rd-party dependencies we‘re using in the project. But we also have a giant bundle with our code. One thing we need to think about is performance in the browser. One of the #1 ways to improve performance is fewer network requests for smaller payloads. Browsers are able to cache files they fetch. Images are usually retrieved via a network request the first time. Subsequent requests go to the browser cache instead. Likewise, JavaScript files are cached. We can probably assume that while our product code will change often, our dependencies more often remain consistent. This means that we could be caching those files and saving the browser network trips. We would also reduce the size of our main bundle containing the product code. Let‘s take a look:

| Name      | Status | Size    |
| --------- | ------ | ------- |
| bundle.js | 200    | 74.4 kB |

To start we have a 74.4kB bundle. This contains lodash and our product code. If we change copy text or a style, the browser would need to fetch the whole bundle for users to see updates. If we imagine a larger app, it‘s easy to see this becoming a bottleneck.

Let‘s separate lodash from our product bundle. We can call all 3rd-party dependencies vendor code, thus `vendor.js`.

| Name             | Status | Size    |
| ---------------- | ------ | ------- |
| vendor.bundle.js | 200    | 73.6 kB |
| main.bundle.js   | 200    | 2.1 kB  |

That‘s a huge reduction in size from our main bundle! Imagine if we were also including others libraries like `react`. Now with browser caching, users would only need to fetch 2.1 kb of files from the server most of the time. One thing to call out is that we download the entire contents of lodash even though we are using a single function. We‘ll come back to this when we talk about tree-shaking.

## Wrapup

Whew, maintaining the names of our bundles and keeping them in sync with `index.html`is a bit of a hassle. Let‘s fix that in the next section.
