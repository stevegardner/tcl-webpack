# 02-basic-webpack-config

We now have a config. This basic config does the same thing that the no-config workflow gave us. But now we can start tweaking the structure of our application. For instance, we could call the output folder `build` instead of `dist`.abs

## Added Webpack config

The first two Webpack config elements are `entry` and `output`.

### `entry`

This is what kicks off the whole application. The entry point to all the code you’ve written. Some older Webpack usage might have done some wild things with `entry`. But with Webpack 5, you‘ll rarely need to have anything other than a string defined.

### `output`

Webpack needs to know what the main bundle should be called. By default, it is called `main.js`. Now that we have the config, it could be called anything. Likewise with the output directory. Webpack needs to know where that bundle should go.

```js
// webpack.config.js

module.exports = {
  // The file which pulls in all the JS for the app. It is the dependency tree ancestor.
  entry: './src/index.js',
  // What gets spit out by Webpack and where does it go.
  output: {
    // Tells Webpack what to name the bundle it creates.
    filename: 'main.js',
    // The directory where the bundle will be saved.
    path: path.resolve(__dirname, 'dist'),
  },
};
```

## Import other file types

What if we want to import a CSS file in `message.js`? It makes sense. We want to couple the component that’s created with some CSS.

We‘ll need a config to tell Webpack how to handle files besides JS and JSON.
