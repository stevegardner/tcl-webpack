import { useState, useRef } from 'react';

export default function LoaderButton() {
  const [MaybeLoaded, setMaybeLoaded] = useState(null);

  const handleClick = async () => {
    const { default: component } = await import(
      /* webpackChunkName: 'AsyncMessage' */ './AsyncMessage'
    );

    setMaybeLoaded(() => component);
  };

  return (
    <>
      <button onClick={handleClick}>Load Async Message</button>
      {!!MaybeLoaded && <MaybeLoaded />}
    </>
  );
}
