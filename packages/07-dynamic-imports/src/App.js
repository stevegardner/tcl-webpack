import LoaderButton from './components/LoaderButton';

export default function App() {
  return (
    <main>
      This is the App!
      <br />
      <LoaderButton />
    </main>
  );
}
