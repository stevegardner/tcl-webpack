# 07-dyanmic-imports

One way we can reduce the size of our bundles (and the amount of code we load) is by using dynamic imports. To use a dynamic import, we use the `import` function which returns a Promise that resolves to the file:

```js
import SyncronousModule from './regular-import';

// vs

const { default: AsyncModule } = await import('./async-import');
```

Webpack will create a separate bundle for modules that are imported dynamically. Dynamic imports are [supported natively](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import) in most browsers. But using Webpack allows us to have better management of bundles and dependencies.

## Magic Comments

Webpack supports a few ["magic" comments](https://webpack.js.org/api/module-methods/#magic-comments) that inform the compiler how to name and load dynamic imports. We’ll talk about one of them here.

### `webpackChunkName`

I've used this one the most. Using this comment explicitly tells Webpack how to name the chunked bundle. This makes for easier debugging.

```js
import (/* webpackChunkName: 'myAsyncModule' */, './myAsyncModule)
```
