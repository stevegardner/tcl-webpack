# 05-dev-server

Webpacks built in dev-server is great for development. Like all things Webpack, its quite configurable as well. We‘re going to look at the built-in dev-server now. Definitely checkout building out a custom dev-server using Express as well.

The dev-server builds our bundles and keeps them in-memory. If we want to see the output of building, we can still run the build script. We can also go to `/webpack-dev-server` in the browser to see the bundles being kept in-memory.

## Stats

To reduce noise, we use `stats: 'errors-only'` in the dev-server config. Otherwise we get a verbose output in the terminal from Webpack that isn't very helpful.

## Source Maps

One thing that we often will see in development is source maps. If we try to debug code that Webpack has compiled, it's a bit obfuscated. The code is hard to read and jumbled into a single line. Source maps provide a link to the source code. From there, we can inspect the code in the browser, as well as drop breakpoints where we'd expect.

Webpack uses the [devtool](https://webpack.js.org/configuration/devtool/#devtool) config to specify how source maps are created. Under the hood, `devtool` names map to instantiating their respective source map plugin. We use `eval-source-map` (which uses [EvalSourceMapDevToolPlugin](https://webpack.js.org/plugins/eval-source-map-dev-tool-plugin/)) to get quality source maps for our compiled code, as recommended by Webpack for development. Different source map plugins have different tradeoffs in speed and quality. In production, we usually don't want to return source maps. But, it is advantageous if the server can be cofigured to allow source maps for staff users.
