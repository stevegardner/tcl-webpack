# No Webpack

This is the basic way to get something onto the web. Just some HTML, JS, and CSS if you'd like it to look nice.

## What‘s nice

Working without bundles can be great for a small, static project. Ideally, this would be something without a lot of JS.

- Very simple
- The browser's module system — import in JS files!

## What‘s not so nice

If you have a lot of JS to work with, these caveats will add up fast.

- The browser's module system - kind of limited…
  - Must specify file extension
  - Only recognizes JS
  - index.js files are treated like a server directory.
- Should probably use IFFE
- Dependencies are global
