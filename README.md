# TCL Talks - Webpack Example Repo

This repo goes along with the slides from the [TCL Webpack Talk](https://docs.google.com/presentation/d/15_2qpTNa9yCh71-sVgjyOVrpD3iq96DU0EUIKxS__ng/edit?usp=sharing). These packages provide examples for using and learning Webpack. Each module has a README explaining the concept of each example.

We start with a project with no webpack at all, and gradually add more features to the build.

## Examples TOC:

0. [No webpack](https://gitlab.com/stevegardner/tcl-webpack/-/tree/main/packages/00-no-webpack)
1. [No-cofig webpack](https://gitlab.com/stevegardner/tcl-webpack/-/tree/main/packages/01-basic-webpack)
1. [Basic config](https://gitlab.com/stevegardner/tcl-webpack/-/tree/main/packages/02-basic-webpack-config)
1. [Manage Assets](https://gitlab.com/stevegardner/tcl-webpack/-/tree/main/packages/03-manage-assets)
1. [Plugins](https://gitlab.com/stevegardner/tcl-webpack/-/tree/main/packages/04-plugins)
1. [Dev-Server](https://gitlab.com/stevegardner/tcl-webpack/-/tree/main/packages/05-dev-server)
1. [React](https://gitlab.com/stevegardner/tcl-webpack/-/tree/main/packages/06-react)
1. [Dynamic imports](https://gitlab.com/stevegardner/tcl-webpack/-/tree/main/packages/07-dynamic-imports)
